package demo2project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@SpringBootApplication
public class Demo2app {

	public static void main(String[] args) {
		SpringApplication.run(Demo2app.class, args);

	}

	@RestController
	public class HelloController {
		
		@RequestMapping("/home")
		public String sayHI() {
			return "Welcome to Springboot Application";
		}

	}
	
}
